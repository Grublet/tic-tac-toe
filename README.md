# Tic Tac Toe

Built with React, a server side rendered application to play Tic Tac Toe. The "AI" uses a MinMax algorithm to figure out the best moves to make.

## Initial setup

- `yarn`

## Development

- `yarn start`
  - Start the dev server at [http://localhost:3000](http://localhost:3000)
- `yarn test`
  - Start `jest` in watch mode

## Production

- `yarn run build && npm run start:prod`
  - Bundle the JS and fire up the Express server for production
  - Build and start a local Docker image in production mode (mostly useful for debugging)

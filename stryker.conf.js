module.exports = function(config) {
  config.set({
    testRunner: "jest",
    mutator: "javascript",
    coverageAnalysis: "off",
    reporters: ['clear-text', 'progress'],
    mutate: ["src/reducers/selector.js"],
    packageManager: 'yarn',
    maxConcurrentTestRunners: 3
  })
}

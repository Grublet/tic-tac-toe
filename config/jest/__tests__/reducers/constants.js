export const gameInSession = {
  'A1': 'X',
  'A2': 'O',
  'A3': 'X',
  'B1': 'O',
  'B2': 'O',
  'B3': null,
  'C1': null,
  'C2': null,
  'C3': null,
}

export const playerXWinsHorizontal = {
  'A1': 'X',
  'A2': 'X',
  'A3': 'X',
  'B1': 'O',
  'B2': 'O',
  'B3': null,
  'C1': null,
  'C2': null,
  'C3': null,
}

export const playerXWinsDiagonal = {
  'A1': 'X',
  'A2': 'X',
  'A3': 'O',
  'B1': 'O',
  'B2': 'X',
  'B3': 'O',
  'C1': 'O',
  'C2': null,
  'C3': 'X',
}

export const playerOWinsDiagonal = {
  'A1': 'X',
  'A2': 'X',
  'A3': 'O',
  'B1': 'X',
  'B2': 'O',
  'B3': 'O',
  'C1': 'O',
  'C2': null,
  'C3': 'X',
}

export const playerOWinsHorizontal = {
  'A1': 'O',
  'A2': 'O',
  'A3': 'O',
  'B1': 'X',
  'B2': 'X',
  'B3': null,
  'C1': null,
  'C2': null,
  'C3': null,
}

export const drawnGame = {
  'A1': 'X',
  'A2': 'X',
  'A3': 'O',
  'B1': 'O',
  'B2': 'O',
  'B3': 'X',
  'C1': 'X',
  'C2': 'O',
  'C3': 'X',
}

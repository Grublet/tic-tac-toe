import { initialState } from '../../../../src/reducers/initialState'

describe('(reducers) initialState - shallow', () => {

  it('loads action dispatchers to props', () => {
    expect(Object.keys(initialState)).toEqual(['grids'])
  })
})

import { calculateWinningGame, winningGame } from '../../../../src/reducers/selector'
import * as constants from './constants'

describe('(Selector)', () => {
  describe('winningGame should', () => {
    it('return correct values on a game which has not ended', () => {
      expect(winningGame({ game: { grids: constants.gameInSession } })).toEqual('')
    })
  })
  describe('calculateWinningGame should', () => {
    it('return correct values on a game which has not ended', () => {
      expect(calculateWinningGame(constants.gameInSession)).toEqual('')
    })
    it('return correct values on a drawn game', () => {
      expect(calculateWinningGame(constants.drawnGame)).toEqual('Draw')
    })
    it('return correct values on horizontal wins', () => {
      expect(calculateWinningGame(constants.playerXWinsHorizontal)).toEqual('X')
      expect(calculateWinningGame(constants.playerOWinsHorizontal)).toEqual('O')
    })
    it('return correct values on diagonal wins', () => {
      expect(calculateWinningGame(constants.playerXWinsDiagonal)).toEqual('X')
      expect(calculateWinningGame(constants.playerOWinsDiagonal)).toEqual('O')
    })
  })
})

import reducer from '../../../../src/reducers/gameReducer'
import { computerMove, playerMove, restartGame } from '../../../../src/actions/gameActions'
import { initialState } from '../../../../src/reducers/initialState'

describe('(Reducer) gameReducer - state', () => {

  beforeEach(() => {
    initialState
  })

  it('should be a function', () => {
    expect(typeof reducer).toBe('function')
  })

  it('should have initialState', () => {
    const action = {}
    const prevState = { ...initialState }
    expect(reducer(undefined, action)).toEqual(prevState)
  })

  it('should handle COMPUTER_MOVE to update the state', () => {
    const action = computerMove('A1')
    const prevState = { ...initialState }
    const nextState = initialState
    nextState.grids['A1'] = 'X'
    expect(reducer(prevState, action).grids['A1']).toEqual(nextState.grids['A1'])
  })

  it('should handle PLAYER_MOVE to update the state', () => {
    const action = playerMove('A1')
    const prevState = { ...initialState }
    const nextState = initialState
    initialState.grids['A1'] = 'O'
    expect(reducer(prevState, action).grids['A1']).toEqual(nextState.grids['A1'])
  })

  it('should handle RESTART_GAME to update the state', () => {
    const action = restartGame()
    const prevState = { ...initialState }
    expect(reducer(undefined, action)).toEqual(prevState)
  })

})

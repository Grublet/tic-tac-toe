import reducer, { rootReducer } from '../../../../src/reducers/index'

describe('(Reducer) index', () => {
  it('should be a function', () => {
    expect(typeof reducer).toBe('function')
  })

  it('should be a function', () => {
    expect(typeof rootReducer({}, {})).toBe('object')
  })
})

import React from 'react'
import { shallow } from 'enzyme'
import { Grid } from '../../../../../src/components/TicTacToe/Grid'
import sinon from 'sinon'

let defaultProps
let wrapper

describe('(Component) TicTacToe/Grid - shallow', () => {
  let historyPushSpy

  beforeAll(() => {
    historyPushSpy = sinon.spy()
  })

  afterEach(() => {
    historyPushSpy.resetHistory()
    wrapper = undefined
  })

  beforeEach(() => {
    defaultProps = {
      value: 'X',
      tileKey: 'A1',
      playerMove: () => { },
      turnToGo: 'X'
    }
    wrapper = shallow(<Grid {...defaultProps} />)
  })

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })
})

import React from 'react'
import { shallow } from 'enzyme'
import TicTacToe from '../../../../../src/components/TicTacToe/TicTacToe'

let defaultProps
let wrapper

describe('(Component) TicTacToe/TicTacToe - shallow', () => {

  beforeEach(() => {
    defaultProps = {
      winningGame: '',
      playerMove: x => x,
      row: 'A',
      game: {}
    }
    wrapper = shallow(<TicTacToe {...defaultProps} />)
  })

  it('matches snapshot for a draw', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('matches snapshot for player X winning', () => {
    wrapper.setProps({
      winningGame: 'X'
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('matches snapshot for player O winning', () => {
    wrapper.setProps({
      winningGame: 'O'
    })
    expect(wrapper).toMatchSnapshot()
  })
  //
  // it('matches snapshot for a draw', () => {
  //   expect(wrapper).toMatchSnapshot()
  // })

})

import React from 'react'
import { shallow } from 'enzyme'
import { RestartGame } from '../../../../../src/components/TicTacToe/RestartGame'

let defaultProps
let wrapper

describe('(Component) TicTacToe/WinningGame - shallow', () => {

  beforeEach(() => {
    defaultProps = {
      winningGame: '',
      restartGameSubmit: x => x
    }
    wrapper = shallow(<RestartGame {...defaultProps} />)
  })

  it('matches snapshot for a draw', () => {
    expect(wrapper).toMatchSnapshot()
  })

  it('matches snapshot for player X winning', () => {
    wrapper.setProps({
      winningGame: 'X'
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('matches snapshot for player O winning', () => {
    wrapper.setProps({
      winningGame: 'O'
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('matches snapshot for a draw', () => {
    wrapper.setProps({
      winningGame: 'Draw'
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('matches snapshot for player default scenario', () => {
    wrapper.setProps({
      winningGame: ''
    })
    expect(wrapper).toMatchSnapshot()
  })
})

import { mapDispatchToProps } from '../../../../../src/components/TicTacToe/index'

describe('(Component) TicTacToe/index - shallow', () => {

  it('loads action dispatchers to props', () => {
    expect(Object.keys(mapDispatchToProps)).toEqual(['playerMove', 'restartGameSubmit'])
  })
})

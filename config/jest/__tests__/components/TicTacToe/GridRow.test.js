import React from 'react'
import { shallow } from 'enzyme'
import { GridRow } from '../../../../../src/components/TicTacToe/GridRow'

let defaultProps
let wrapper

describe('(Component) TicTacToe/GridRow - shallow', () => {

  beforeEach(() => {
    defaultProps = {
      game: {},
      playerMove: () => { },
      row: 'A'
    }
    wrapper = shallow(<GridRow {...defaultProps} />)
  })

  it('matches snapshot', () => {
    expect(wrapper).toMatchSnapshot()
  })

})

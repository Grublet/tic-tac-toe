import sagaHelper from 'redux-saga-testing'
import rootsaga from '../../../../src/saga/saga'
import {  restartGame, watchComputerMove, playerMove, computerMove, watchPlayerMove, watchRestartGame }
from '../../../../src/saga/saga'

describe('Rootsaga should yield functions', () => {

  const RootSagaTest = (result) => {
    expect(Object.keys(result)).toEqual(['@@redux-saga/IO', 'combinator', 'type', 'payload'])
    expect(typeof result).toEqual('object')
    expect(typeof result).toEqual('object')
  }

  const it = sagaHelper(rootsaga())
  it('watchComputerMove', result => {
    RootSagaTest(result, watchComputerMove)
  })

  it('watchPlayerMove', result => {
    RootSagaTest(result, watchPlayerMove)
  })

  it('watchRestartGame', result => {
    RootSagaTest(result, watchRestartGame)
  })
})

describe('Rootsaga should yield functions', () => {

  const forkTest = (result) => {
      expect(typeof result).toEqual('object')
      expect(typeof result).toEqual('object')
    }

  const it = sagaHelper(rootsaga())
  it('computerMove', result => {
    forkTest(result, computerMove)
  })

  it('playerMove', result => {
    forkTest(result, playerMove)
  })

  it('restartGame', result => {
    forkTest(result, restartGame)
  })
})

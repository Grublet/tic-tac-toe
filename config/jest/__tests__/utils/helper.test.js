import * as helper  from '../../../../src/utils/helper'


describe('(utils) helper', () => {
  it('evenNumberOfNullValues', () => {
    expect(helper.evenNumberOfNullValues({ 'A1': null })).toEqual(false)
    expect(helper.evenNumberOfNullValues({ 'A1': null, 'A2': null })).toEqual(true)
  })

  it('nullValues', () => {
    expect(helper.nullValues({ 'A1': null })).toEqual([null])
    expect(helper.nullValues({ 'A1': null, 'A2': 'X' })).toEqual([null])
  })

  it('getTileValue', () => {
    expect(helper.getTileValue({ 'A1': null })).toEqual('X')
    expect(helper.getTileValue({ 'A2': 'X' })).toEqual('O')
  })
})

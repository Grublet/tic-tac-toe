import * as Actions from '../../../../src/actions/gameActions'
import * as Constants from '../../../../src/reducers/constants'

describe('actions/gameActions', () => {

  it('playerMoveSubmit returns the correct values', () => {
    expect(Actions.computerMove()).toEqual({
      'type': Constants.COMPUTER_MOVE
    })
  })

  it('playerMoveSubmit returns the correct values', () => {
    expect(Actions.playerMoveSubmit({ tile: 'A1', 'piece': 'O' })).toEqual({
      'payload': { 'piece': 'O', 'tile': 'A1' }, 'type': Constants.PLAYER_MOVE_SUBMIT
    })
  })

  it('playerMove returns the correct values', () => {
    expect(Actions.playerMove({ tile: 'A1', 'piece': 'O' })).toEqual({
      'payload': { 'piece': 'O', 'tile': 'A1' }, 'type': Constants.PLAYER_MOVE
    })
  })

  it('restartGameSubmit returns the correct values', () => {
    expect(Actions.restartGameSubmit()).toEqual({
      'type': Constants.RESTART_GAME_SUBMIT
    })
  })

  it('restartGame returns the correct values', () => {
    expect(Actions.restartGame({ tile: 'A1', 'piece': 'O' })).toEqual({
      'type': Constants.RESTART_GAME
    })
  })

})

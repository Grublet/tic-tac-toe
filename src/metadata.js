export const title = 'React SSR Boilerplate'

export const meta = [
  {
    charset: 'UTF-8'
  },
  {
    name: 'viewport',
    content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
  },
  {
    httpEquiv: 'X-UA-Compatible',
    content: 'IE=edge'
  },
  {
    name: 'description',
    content: 'Boilerplate for React apps with routing & server side rendering'
  }
]

export const link = [
  {
    rel: 'shortcut icon',
    href: `${process.env.PUBLIC_URL}/favicon.ico?v1`
  },
  {
    rel: "stylesheet",
    href: "https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
    integrity: "sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T",
    crossorigin: "anonymous"
  }
]

export const script = []

export const noscript = []

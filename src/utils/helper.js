//@flow
import { filter, modulo, isNil, values, length } from 'ramda'

export const evenNumberOfNullValues = (game:Object) => modulo(length(nullValues(game)), 2) === 0
export const nullValues = (game:Object) => filter(x => isNil(x), values(game))

// If the number of null values is even, 0 plays, if its odd
// then X plays
export const getTileValue = (grids:Object) => evenNumberOfNullValues(grids) ? 'O' : 'X'

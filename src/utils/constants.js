export const diagonalLeft = ['A1', 'B2', 'C3']
export const diagonalRight = ['A3', 'B2', 'C1']

export const HUMAN_PLAYS_FIRST = 'Human plays first'

export const HEADER_TEXT = {
  'X': 'Human Wins',
  'O': 'Computer Wins',
  'Draw': 'Draw'
}

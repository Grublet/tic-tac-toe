import thunk from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger'
import createSagaMiddleware from 'redux-saga'
import watchGame from '../saga/saga'
import rootReducer from '../reducers'

const configureStore = (initialState, options = { logger: true }) => {
  const middleware = [thunk]
  const sagaMiddleware = createSagaMiddleware()
  middleware.push(sagaMiddleware)

  if (process.env.NODE_ENV !== 'production' && options.logger) {
    const logger = createLogger()
    middleware.push(logger)
  }

  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middleware)
  )

  if (process.env.NODE_ENV !== 'test') {
    sagaMiddleware.run(watchGame)
  }
  return store
}

export default configureStore

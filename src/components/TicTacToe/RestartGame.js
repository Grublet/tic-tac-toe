//@flow
import React from 'react'
import { HEADER_TEXT, HUMAN_PLAYS_FIRST } from '../../utils/constants'

export const RestartGame = (props: RestartGameType) => {
  const { winningGame, restartGameSubmit } = props
  const text = HEADER_TEXT[winningGame]
  const displayText = text === undefined ? HUMAN_PLAYS_FIRST : HEADER_TEXT[winningGame]

  return (
    <div className={'winningGameWrapper'}>
      <h1>{displayText}</h1>
      <button onClick={() => restartGameSubmit()}
        className='restartGameButton'>
        RESTART GAME
     </button>
    </div>
  )
}

type RestartGameType = {
  winningGame: string,
  restartGameSubmit: Function
}

//@flow
import React from 'react'
import { Grid } from './Grid'
import { Row, Col } from 'react-bootstrap'

export const GridRow = (props: GridRowProps) => {
  const { game, playerMove, row } = props
  return (
    <Row className={'gridRow'}>
      <Col />
      <Col lg={12} md={12} sm={12} xs={12}>
        <Grid value={game[row+'1']} tileKey={row+'1'} playerMove={playerMove} />
        <Grid value={game[row+'2']} tileKey={row+'2'} playerMove={playerMove} />
        <Grid value={game[row+'3']} tileKey={row+'3'} playerMove={playerMove} />
      </Col>
    </Row>
  )
}

type GridRowProps = {
  game: Object,
  playerMove: Function,
  row: string
}

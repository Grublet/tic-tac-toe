//@flow
import React, { Component } from 'react'
import { GridRow } from './GridRow'
import { Row, Col } from 'react-bootstrap'
import { RestartGame } from './RestartGame'

class TicTacToe extends Component<TicTacToeProps> {

  render() {
    const { game, winningGame, restartGameSubmit } = this.props
    const { playerMove } = this.props
    const pMove = winningGame === '' ? playerMove : () => { }
    return (
      <div className={'ticTacToeWrapper'}>
        <Row>
          <Col lg={4} md={4} sm={4} />
          <Col lg={4} md={4} sm={4}>
          </Col>
          <Col lg={4} md={4} sm={4} xs={12} />

        </Row>
        <div className={'gridWrapper'}>
          <RestartGame winningGame={winningGame} restartGameSubmit={restartGameSubmit} />
          <GridRow game={game} row={'A'} playerMove={pMove} />
          <GridRow game={game} row={'B'} playerMove={pMove} />
          <GridRow game={game} row={'C'} playerMove={pMove} />
        </div>
      </div>
    )
  }
}

type TicTacToeProps = {
  restartGameSubmit: Function,
  game: Object,
  playerMove: Function,
  winningGame: string
}

export default TicTacToe

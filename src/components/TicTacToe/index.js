//@flow
import { connect } from 'react-redux'
import TicTacToe from './TicTacToe'
import { playerMoveSubmit, restartGameSubmit } from '../../actions/gameActions'
import { winningGame } from '../../reducers/selector'

export const mapStateToProps = (state: Object) => {
  return {
    winningGame: winningGame(state),
    game: state.game.grids
  }
}

export const mapDispatchToProps = {
  playerMove: playerMoveSubmit,
  restartGameSubmit: restartGameSubmit
}

export default connect(mapStateToProps, mapDispatchToProps)(TicTacToe)

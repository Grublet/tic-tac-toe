//@flow
import React from 'react'
import { isNil } from 'ramda'

export const Grid = (props: GridProps) => {
  const { value, tileKey, playerMove } = props
  const hide = isNil(value) ? 'hide' : 'show'

  return (
    <button className={`button ${hide}`}
      onClick={() => isNil(value) ? playerMove(tileKey) : ''}>
      {isNil(value) ? '#' : value}
    </button>
  )
}

type GridProps = {
  tileKey: string,
  value: string,
  playerMove: Function
}

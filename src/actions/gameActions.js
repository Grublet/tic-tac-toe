//@flow
import * as Constants from '../reducers/constants'

export const computerMove = () => ({
  type: Constants.COMPUTER_MOVE
})

export const playerMoveSubmit = (payload: string) => ({
  type: Constants.PLAYER_MOVE_SUBMIT,
  payload
})

export const playerMove = (payload: string) => ({
  type: Constants.PLAYER_MOVE,
  payload
})

export const restartGameSubmit = () => ({
  type: Constants.RESTART_GAME_SUBMIT
})

export const restartGame = () => ({
  type: Constants.RESTART_GAME
})

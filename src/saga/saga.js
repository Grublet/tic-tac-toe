//@flow
import { put, takeEvery, fork } from 'redux-saga/effects'
import * as Constants from '../reducers/constants'
import * as Actions from '../actions/gameActions'
import type { Saga } from 'redux-saga'

export function* watchComputerMove(): Saga<void> {
  yield takeEvery(Constants.PLAYER_MOVE, computerMove)
}

export function* computerMove(): Saga<void> {
  yield put(Actions.computerMove())
}

export function* watchPlayerMove(): Saga<void> {
  yield takeEvery(Constants.PLAYER_MOVE_SUBMIT, playerMove)
}

export function* playerMove(action: Object): Saga<void> {
  yield put(Actions.playerMove(action.payload))
}

export function* watchRestartGame(): Saga<void> {
  yield takeEvery(Constants.RESTART_GAME_SUBMIT, restartGame)
}

export function* restartGame(): Saga<void> {
  yield put(Actions.restartGame())
}


export default function* watchGame(): Saga<void> {
  yield fork(watchComputerMove)
  yield fork(watchPlayerMove)
  yield fork(watchRestartGame)
}

//@flow
export const PLAYER_MOVE_SUBMIT = 'PLAYER_MOVE_SUBMIT'
export const PLAYER_MOVE = 'PLAYER_MOVE'
export const RESTART_GAME_SUBMIT = 'RESTART_GAME_SUBMIT'
export const RESTART_GAME = 'RESTART_GAME'
export const COMPUTER_MOVE = 'COMPUTER_MOVE'
export const COMPUTER_MOVE_SUBMIT = 'COMPUTER_MOVE_SUBMIT'

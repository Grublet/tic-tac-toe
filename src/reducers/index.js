//@flow
import { combineReducers } from 'redux'

import game from './gameReducer'

const appReducer = combineReducers({
  game
})

export const rootReducer = (state: Object, action: Object) => {
  return appReducer(state, action)
}

export default rootReducer

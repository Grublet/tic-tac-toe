import { isEmpty, isNil, filter, keys } from 'ramda'
import { calculateWinningGame } from './selector'

export const vacantTiles = (game: Object) => filter(x => isNil(x) === true, game)

export function miniMax(board: Object, player: string) {
  const availSpots = vacantTiles(board)
  if (isEmpty(availSpots)) {
    return { score: 0 }
  }
  const win = calculateWinningGame(board)
  let availSpotsArray = keys(availSpots)
  if (win === 'X') {
    return { score: -10 }
  }
  else if (win === 'O') {
    return { score: 10 }
  }
  // an array to collect all the objects
  let moves = []
  // loop through available spots
  for (let i = 0; i < availSpotsArray.length; i++) {
    //create an object for each and store the index of that spot
    let move = {}
    move.index = availSpotsArray[i]

    // set the empty spot to the current player
    let temp = availSpotsArray[i]
    board[temp] = player
    /*collect the score resulted from calling minimax
      on the opponent of the current player*/
    if (player == 'O') {
      let result = miniMax(board, 'X')
      move.score = result.score
    }
    else {
      let result = miniMax(board, 'O')
      move.score = result.score
    }

    // reset the spot to empty
    board[availSpotsArray[i]] = move.index
    // push the object to the array
    moves.push(move)

  }

  var bestMove
  if (player === 'O') {
    let bestScore = -10000
    for (let i = 0; i < moves.length; i++) {
      if (moves[i].score > bestScore) {
        bestScore = moves[i].score
        bestMove = i
      }
    }
  }
  else {
    let bestScore = 10000
    for (let i = 0; i < moves.length; i++) {
      if (moves[i].score < bestScore) {
        bestScore = moves[i].score
        bestMove = i
      }
    }
  }
  // return the chosen move (object) from the moves array
  return moves[bestMove]
}

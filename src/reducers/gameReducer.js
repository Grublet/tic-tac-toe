import { initialState } from './initialState'
import * as Constants from './constants'
import { getTileValue } from '../utils/helper'
import  { miniMax } from './helper'
  /*eslint-disable */
export default (state: Object = initialState, action: Object) => {
  let newState
  switch (action.type) {
    case Constants.PLAYER_MOVE:
     newState =  {...state,
        grids: {
           ...state.grids,
          }
      }
      newState.grids[action.payload] = getTileValue(state.grids)
      return newState
    case Constants.COMPUTER_MOVE:
     newState =  {...state,
        grids: {
           ...state.grids
          }
      }
      let value = getTileValue(state.grids)
      let key = miniMax(state.grids, 'O').index
      newState.grids[key] = getTileValue(state.grids)
      return newState
    case Constants.RESTART_GAME:
      return initialState
    default:
      return state
  }
}

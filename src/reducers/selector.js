//@flow
import { createSelector } from 'reselect'
import { any, isEmpty, all, tail, identity, pipe, countBy, map, head, keys, filter, values } from 'ramda'
import { diagonalLeft, diagonalRight } from '../utils/constants'
import { vacantTiles } from './helper'

const game = state => state.game

export const checkDiagonalWin = (keys: Object) => {
  const diagonalRightWin = all(x => keys.includes(x), diagonalRight)
  const diagonalLeftWin = all(x => keys.includes(x), diagonalLeft)
  return diagonalRightWin || diagonalLeftWin
}

export const checkWin = (keys: Object) => {
  const wonViaDiagonal = checkDiagonalWin(keys)
  if (wonViaDiagonal) {
    return true
  }
  const rowValues = map(x => head(x), keys)
  const rowDuplicates = countDuplicates(rowValues)
  const winningRow = any(x => x === 3, rowDuplicates)

  const columnValues = map(x => tail(x), keys)
  const columnDuplicates = countDuplicates(columnValues)
  const winningColumn = any(x => x === 3, columnDuplicates)

  return winningRow || winningColumn
}

const countDuplicates = (keys: Object) => {
  const countDupes = pipe(
    countBy(identity),
    values
  )
  return countDupes(keys)
}

export const playerValues = (game: Object, value: string) => keys(filter(x => x === value, game))

export const calculateWinningGame = (game: Object) => {
  const playerXValues = playerValues(game, 'X')
  const playerOValues = playerValues(game, 'O')

  const playerXWins = checkWin(playerXValues)
  const playerOWins = checkWin(playerOValues)

  if (playerXWins) {
    return 'X'
  }
  else if (playerOWins) {
    return 'O'
  }


  const draw = isEmpty(vacantTiles(game))
  if (draw) {
    return 'Draw'
  }
  return ''
}

export const winningGame = createSelector(
  game,
  (game) => calculateWinningGame(game.grids)
)
